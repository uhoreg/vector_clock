/* global exports */
/* Copyright 2020 Hubert Chathi <hubert@uhoreg.ca>
 *
 *  DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                    Version 2, December 2004
 *
 * Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 */
"use strict";

exports.timeInTimeZoneImpl = function(Nothing, Time, timezone, date) {
    try {
        const timeArr = (new Intl.DateTimeFormat(
            "en-US",
            {
                hour: "2-digit",
                minute: "2-digit",
                hour12: false,
                timeZone: timezone
            }).format(date))
              .split(":")
              .map(function (x) { return parseInt(x); });
        return Time(timeArr[0])(timeArr[1]);
    } catch (e) {
        return Nothing;
    }
};

var parts = [];

exports.setParts = function(p) {
    return function() {
        parts = p;
    };
};

exports.getParts = function() {
    return parts;
};
