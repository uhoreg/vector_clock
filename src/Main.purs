-- Copyright 2020 Hubert Chathi <hubert@uhoreg.ca>

--  DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
--                    Version 2, December 2004

-- Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

-- Everyone is permitted to copy and distribute verbatim or modified
-- copies of this license document, and changing it is allowed as long
-- as the name is changed.

--            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
--   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

--  0. You just DO WHAT THE FUCK YOU WANT TO.

module Main where

import Prelude

import Data.Array (catMaybes)
import Data.Char (fromCharCode)
import Data.Function.Uncurried (Fn4, runFn4)
import Data.Int (floor, toNumber)
import Data.JSDate (JSDate, now, getMilliseconds, getSeconds)
import Data.Maybe (Maybe(..), maybe, fromJust)
import Data.String (singleton, codePointFromChar)
import Data.Traversable (traverse, traverse_)

import Effect (Effect)
-- import Effect.Console (log)
import Effect.Timer (setTimeout)

import Math (sin, cos, pi)

import Partial.Unsafe (unsafePartial)

import Web.HTML (window)
import Web.HTML.Window as W
import Web.HTML.Window (document)
import Web.HTML.HTMLDocument (toNonElementParentNode, toDocument)

import Web.DOM.Document (Document, createElementNS, createElement)
import Web.DOM.Element as E
import Web.DOM.Element (Element)
import Web.DOM.Node (Node, appendChild, textContent, setTextContent, childNodes)
import Web.DOM.NodeList as NodeList
import Web.DOM.NonElementParentNode (getElementById)
import Web.DOM.ParentNode (QuerySelector(..), querySelector)

import Web.Event.Event (EventType(..))
import Web.Event.EventTarget (addEventListener, eventListener)

-- configuration for a time zone
type TZConfig = { name :: String
                , tz :: String
                }

--------
-- Calculations based on a time -- pure functions
--------

-- time represented in hours (24h) and minutes
type HourMinutes = { hours :: Int, minutes :: Int }

makeHourMinutes :: Int -> Int -> HourMinutes
-- some browsers *cough*Chrome*cough* seem to think that 24:xx is correct rather
-- than 00:xx for the midnight hour.  So taking the hour mod 24 fixes this.
makeHourMinutes h m = { hours: (h `mod` 24), minutes: m}

foreign import timeInTimeZoneImpl :: forall t. Fn4 t (Int -> Int -> t) String JSDate t

-- get the HourMinutes, in a given timezone, of the given time
timeInTimeZone :: String -> JSDate -> Maybe HourMinutes
timeInTimeZone = runFn4 timeInTimeZoneImpl Nothing justTime
  where justTime h m = Just $ makeHourMinutes h m

-- calculate the angle (in degrees) of the hour hand, given an HourMinutes
-- The angle will range from 0 (00:00) to ~720 (23:59)
hourMinutesToAngle :: HourMinutes -> Int
hourMinutesToAngle t = (t.hours * 60 + t.minutes) / 2


type LabelPosition = { x :: Number, y :: Number }

-- calculate the top-left coordinates of a label, given the angle of the hand
labelPosFromAngle :: Number -> Number -> Number -> Int -> LabelPosition
labelPosFromAngle width height radius angle = calculate
  where angle' = angle `mod` 360
        -- calculate the proportion of base, depending on where value is in [min, max]
        prop base min max value = base * (toNumber $ value - min) / (toNumber $ max - min)
        radians = (toNumber angle') * pi / 180.0
        basex = (sin radians) * radius
        basey = -(cos radians) * radius
        vertCutoff = 30
        calculate
          | 0 <= angle' && angle' <= vertCutoff =
            { x: basex - (prop width vertCutoff (-vertCutoff) angle')
            , y: basey - height }
          | vertCutoff <= angle' && angle' <= (180 - vertCutoff) =
            { x: basex
            , y: basey - (prop height (180 - vertCutoff) vertCutoff angle') }
          | (180 - vertCutoff) <= angle' && angle' <= (180 + vertCutoff) =
            { x: basex - (prop width (180 - vertCutoff) (180 + vertCutoff) angle')
            , y: basey }
          | (180 + vertCutoff) <= angle' && angle' <= (360 - vertCutoff) =
            { x: basex - width
            , y: basey - (prop height (180 + vertCutoff) (360 - vertCutoff) angle') }
          | otherwise =
            { x: basex - (prop width (360 + vertCutoff) (360 - vertCutoff) angle')
            , y: basey - height }

-- the text to show on a label
labelText :: String -> HourMinutes -> String
labelText name hm = name <> " - " <> (dblDgt hm.hours) <> ":" <> (dblDgt hm.minutes)
  where dblDgt x
          | x < 10 = "0" <> show x
          | otherwise = show x

hexColourFromAngle :: Int -> String
hexColourFromAngle angle = calculate
  where angle' = angle `mod` 360
        hexDigit d
          | d < 10 = show d
          | otherwise = singleton $ codePointFromChar $ unsafePartial $ fromJust $ fromCharCode (55 + d)
        hex c = (hexDigit (c / 16)) <> (hexDigit (c `mod` 16))
        hexColour r g b = "#" <> (hex r) <> (hex g) <> (hex b)
        maxVal = 0xAA
        prop base min max value = base * (value - min) / (max - min)
        calculate
          | 0 <= angle' && angle' <= 60 = hexColour maxVal (prop maxVal 0 60 angle') 0
          | 60 <= angle' && angle' <= 120 = hexColour (prop maxVal 120 60 angle') maxVal 0
          | 120 <= angle' && angle' <= 180 = hexColour 0 maxVal (prop maxVal 120 180 angle')
          | 180 <= angle' && angle' <= 240 = hexColour 0 (prop maxVal 240 180 angle') maxVal
          | 240 <= angle' && angle' <= 300 = hexColour (prop maxVal 240 300 angle') 0 maxVal
          | 300 <= angle' && angle' <= 360 = hexColour maxVal 0 (prop maxVal 360 300 angle')
          | otherwise = "#FFFFFF" -- should never happen

--------
-- Manipulate clock elements - no global state manipulation other than DOM
--------

-- clock parts related to each time zone
-- each time zone is associated with an hour hand and a label
type TZParts = { name :: String
               , tz :: String
               , hand :: Element
               , label :: Element
               }

setAngle :: Element -> Int -> Effect Unit
setAngle el angle = E.setAttribute "transform" ("rotate(" <> show angle <> ")") el

setColour :: Element -> String -> Effect Unit
setColour el colour = E.setAttribute "stroke" colour el

setLabelText :: Element -> String -> HourMinutes -> Effect Unit
setLabelText label name hm = void $ setTextContent (labelText name hm) (E.toNode label)

setLabelPosition :: Element -> Number -> Int -> Effect Unit
setLabelPosition label radius angle = do
  width <- E.clientWidth label
  height <- E.clientHeight label
  let pos = labelPosFromAngle width height radius (angle `mod` 360)
  E.setAttribute "style" ("left: " <> (show pos.x) <> "px; top: " <> (show pos.y) <> "px;") label

createHand :: Node -> Document -> Effect Element
createHand container d = do
  line <- createElementNS (Just "http://www.w3.org/2000/svg") "line" d
  E.setAttribute "x1" "0" line
  E.setAttribute "y1" "0" line
  E.setAttribute "x2" "0" line
  E.setAttribute "y2" "-65" line
  E.setAttribute "stroke" "black" line
  void $ appendChild (E.toNode line) container
  pure line

createLabel :: Node -> Document -> String -> Effect Element
createLabel container d name = do
  label <- createElement "div" d
  void $ appendChild (E.toNode label) container
  setTextContent (name <> " - XX:XX") (E.toNode label)
  pure label

-- create the parts of the clock for each time zone
createParts :: Array TZConfig -> Effect (Array TZParts)
createParts tzconfigs = do
  d <- window >>= document
  maybeHands <- getElementById "hands" (toNonElementParentNode d)
  let handContainer = E.toNode $ unsafePartial $ fromJust maybeHands
  maybeLabels <- getElementById "labels" (toNonElementParentNode d)
  let labelContainer = E.toNode $ unsafePartial $ fromJust maybeLabels

  let d' = toDocument d
  traverse (\ tzcfg -> do
               h <- createHand handContainer d'
               l <- createLabel labelContainer d' tzcfg.name
               pure { name: tzcfg.name, tz: tzcfg.tz, hand: h, label: l }
           ) tzconfigs

--------
-- Clock management - affects global state
--------

-- get the time zones to be displayed
getTimeZones :: Effect (Array TZConfig)
getTimeZones = do
  d <- window >>= document <#> toNonElementParentNode
  maybeTZContainer <- getElementById "timezones" d
  let tzContainer = unsafePartial $ fromJust maybeTZContainer
  children <- childNodes (E.toNode tzContainer) >>= NodeList.toArray

  let sel = QuerySelector ".tzlabel"

  -- iterate over the children of #timezones
  traverse (\ node -> do
               let maybeElem = E.fromNode node
               maybe (pure Nothing)
                 (\ el -> do
                     -- Get the time zone from the "data-tz" attribute, and the
                     -- timezone name from the descendant with class "tzlabel".
                     maybeTz <- E.getAttribute "data-tz" el
                     maybeTzLabel <- querySelector sel (E.toParentNode el)
                     maybeTzName <- maybe (pure Nothing) (\ e -> textContent (E.toNode e) <#> Just) maybeTzLabel
                     -- If they both exist, return a TZConfig.
                     pure $ (\ name tz -> { name: name, tz: tz }) <$> maybeTzName <*> maybeTz
                 )
                 maybeElem
           ) children
    <#> catMaybes

foreign import setParts :: Array TZParts -> Effect Unit
foreign import getParts :: Effect (Array TZParts)

-- move all the clock elements
redraw :: Effect Unit
redraw = do
  partsArr <- getParts

  date <- now

  d <- window >>= document <#> toNonElementParentNode
  maybeClock <- getElementById "clock" d
  let clock = unsafePartial $ fromJust maybeClock
  clockHeight <- E.clientHeight clock
  let radius = clockHeight / 2.0

  let utcAngle = hourMinutesToAngle $ unsafePartial $ fromJust $ timeInTimeZone "UTC" date

  -- FIXME: if different timezones have the same angle, combine them into one
  traverse_ (\parts -> do
                let maybeHm = timeInTimeZone parts.tz date
                -- FIXME: hide parts if hm is Nothing
                maybe (pure unit)
                  (\hm -> do
                      let angle = hourMinutesToAngle hm
                      setAngle parts.hand angle
                      setColour parts.hand $ hexColourFromAngle (angle - utcAngle)
                      setLabelText parts.label parts.name hm
                      setLabelPosition parts.label (radius + 10.0) angle
                  ) maybeHm
            ) partsArr


-- run a function at the beginning of every minute
loopEveryMinute :: Effect Unit -> Effect Unit
loopEveryMinute f = do
  f
  -- get current time, find how much time until the next minute (add some
  -- padding to make sure we're past the minute mark), and wait for that long
  -- before recursing
  currTime <- now
  milliseconds <- getMilliseconds currTime <#> floor
  seconds <- getSeconds currTime <#> floor
  let millisecondsRemaining = (1000 - milliseconds) + (60 - seconds) * 1000
  void $ setTimeout (millisecondsRemaining + 10) do
    loopEveryMinute f

toggleMenu :: Element -> Effect Unit
toggleMenu menu = do
  cls <- E.className menu
  if (cls == "expanded")
    then do E.setClassName "" menu
    else do E.setClassName "expanded" menu

-- set up the clock and make sure it gets redrawn at appropriate times
init :: Effect Unit
init = do
  getTimeZones >>= createParts >>= setParts

  loopEveryMinute redraw

  -- redraw when the window resizes
  w <- window <#> W.toEventTarget
  listener <- eventListener (\ _ -> redraw)
  addEventListener (EventType "resize") listener false w

  -- toggle the menu when the button is clicked
  d <- window >>= document <#> toNonElementParentNode
  maybeMenu <- getElementById "menu" d
  let menu = unsafePartial $ fromJust maybeMenu
  maybeMenuButton <- getElementById "menu-button" d
  let menuButton = unsafePartial $ fromJust maybeMenuButton
  clickListener <- eventListener (\ _ -> toggleMenu menu)
  addEventListener (EventType "click") clickListener false (E.toEventTarget menuButton)

main :: Effect Unit
main = do
  w <- window <#> W.toEventTarget
  loadListener <- eventListener (\ _ -> init)
  addEventListener (EventType "load") loadListener false w
